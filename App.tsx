import React from "react";
import { SafeAreaView, StatusBar } from "react-native";
import { GestureHandlerRootView } from "react-native-gesture-handler";
import SystemNavigationBar from "react-native-system-navigation-bar";

import SecondView from "./src/screens/SecondView";

SystemNavigationBar.immersive();

// const pkg = require("./package.json");

function App(): React.JSX.Element {
    // const appVersion = pkg.version;

    return (
        <GestureHandlerRootView>
            <SafeAreaView>
                <StatusBar hidden />
                {/* <MainView /> */}
                <SecondView />
            </SafeAreaView>
        </GestureHandlerRootView>
    );
}

export default App;
