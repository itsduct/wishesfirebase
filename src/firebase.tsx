// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCNFJb1GfkzenYC8kkSZLh_Oj3MQDV6bwY",
    authDomain: "wishes-5c9f5.firebaseapp.com",
    projectId: "wishes-5c9f5",
    storageBucket: "wishes-5c9f5.appspot.com",
    messagingSenderId: "42842198406",
    appId: "1:42842198406:web:33669a80668b0e8ac39c07",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// init firestore
export const db = getFirestore(app);
