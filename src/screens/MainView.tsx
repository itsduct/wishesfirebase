import React, { FC, useCallback, useMemo, useState } from "react";
import styles from "./main.module";
import { TextInput, View, TouchableOpacity, Text } from "react-native";
import { db } from "../firebase";
import { addDoc, collection } from "firebase/firestore";
import LinearGradient from "react-native-linear-gradient";
import Video from "react-native-video";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const MainView: FC = () => {
    const [name, setName] = useState("");
    const [text, setText] = useState("");

    const clear = useCallback(() => {
        setName("");
        setText("");
    }, []);

    const addWish = useCallback(async () => {
        try {
            console.log("addWish");
            const docRef = await addDoc(collection(db, "wishes"), {
                name,
                text,
            });

            if (docRef.id) {
                clear();
            }
        } catch (error) {
            console.log(error);
        }
    }, [clear, name, text]);

    const handlePress = () => {
        addWish();
    };

    const disabledMemo = useMemo(() => !name || !text, [name, text]);

    return (
        <KeyboardAwareScrollView contentContainerStyle={styles.container}>
            <View style={styles.leftContainer}>
                <Video
                    source={require("../assets/1440x900_v3.mp4")}
                    style={styles.backgroundVideo}
                    muted={true}
                    repeat={true}
                    resizeMode={"cover"}
                    rate={1.0}
                    ignoreSilentSwitch={"obey"}
                />
                <View style={styles.overlay}>
                    <Text style={styles.leftHeading}>Елена{"\n"}Гущина</Text>
                    <Text style={styles.leftFooter}>
                        Уважаемые друзья, мы предлагаем Вам написать тёплые пожелания для
                        именинницы, мы обязательно транслируем их на экраны и все вместе поздравим
                        Елену в этот замечательный день!
                    </Text>
                </View>
            </View>

            <View style={styles.rightContainer}>
                <Text style={styles.subheading}>Написать пожелание можно здесь:</Text>

                <View style={styles.inputWrapper}>
                    <LinearGradient
                        colors={["#49CCEC", "#A422C9"]}
                        style={[styles.gradientBorder]}
                        useAngle>
                        <TextInput
                            style={[styles.input, styles.name]}
                            placeholder="Имя"
                            placeholderTextColor="#aaa"
                            value={name}
                            onChangeText={setName}
                        />
                    </LinearGradient>
                </View>

                <View style={styles.inputWrapper}>
                    <LinearGradient
                        colors={["#49CCEC", "#A422C9"]}
                        style={styles.gradientBorder}
                        useAngle>
                        <TextInput
                            style={[styles.input, styles.textArea]}
                            placeholder="Текст"
                            placeholderTextColor="#aaa"
                            multiline
                            value={text}
                            onChangeText={setText}
                        />
                    </LinearGradient>
                </View>

                <TouchableOpacity
                    style={[styles.button, disabledMemo ? styles.buttonDisabled : undefined]}
                    onPress={handlePress}
                    disabled={disabledMemo}>
                    <LinearGradient
                        colors={["#48CDEC", "#A61DC8"]}
                        style={styles.gradient}
                        useAngle>
                        <Text style={styles.buttonText}>ОТПРАВИТЬ</Text>
                    </LinearGradient>
                </TouchableOpacity>
                <View style={styles.heading}>
                    <Text style={styles.brand}>Club 55</Text>
                    <Text
                        style={[
                            styles.brand,
                            styles.shadow,
                            { textShadowOffset: { width: -2, height: -2 } },
                        ]}>
                        Club 55
                    </Text>
                    <Text
                        style={[
                            styles.brand,
                            styles.shadow,
                            { textShadowOffset: { width: -2, height: 2 } },
                        ]}>
                        Club 55
                    </Text>
                    <Text
                        style={[
                            styles.brand,
                            styles.shadow,
                            { textShadowOffset: { width: 2, height: 2 } },
                        ]}>
                        Club 55
                    </Text>
                </View>
            </View>
        </KeyboardAwareScrollView>
    );
};

export default MainView;
