import React, { FC, useCallback, useEffect, useMemo, useState } from "react";
import { View, Text, TextInput, TouchableOpacity, ActivityIndicator } from "react-native";
import Video from "react-native-video";
import styles from "./second.module.ts";
import { addDoc, collection } from "firebase/firestore";
import { db } from "../firebase.tsx";
import LinearGradient from "react-native-linear-gradient";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

const SecondView: FC = () => {
    const [name, setName] = useState("");
    const [text, setText] = useState("");
    const [showingKeyboard, setShowingKeyboard] = useState(false);
    const [sent, setSent] = useState<{ done: boolean; loading: boolean }>({
        done: false,
        loading: false,
    });

    useEffect(() => {
        if (sent.done) {
            setTimeout(() => {
                setSent((state) => ({
                    ...state,
                    done: false,
                }));
            }, 3000);
        }
    }, [sent.done]);

    const clear = useCallback(() => {
        setName("");
        setText("");
    }, []);

    const addWish = useCallback(async () => {
        try {
            const docRef = await addDoc(collection(db, "wishes"), {
                name,
                text,
            });

            if (docRef.id) {
                clear();
                setSent((state) => ({
                    ...state,
                    done: true,
                    loading: false,
                }));
            }
        } catch (error) {
            console.log(error);
            setSent((state) => ({
                ...state,
                done: false,
                loading: false,
            }));
        }
    }, [clear, name, text]);

    const handlePress = () => {
        if (!sent.done && !sent.loading) {
            setSent((state) => ({
                ...state,
                loading: true,
            }));
            addWish();
        }
    };
    const disabledMemo = useMemo(
        () => !name || !text || sent.done || sent.loading,
        [name, sent.done, sent.loading, text],
    );

    return (
        <>
            <Video
                source={require("../assets/1440x900_v3_blured.mp4")}
                style={styles.absolute}
                muted={true}
                repeat={true}
                resizeMode="cover"
                rate={1.0}
                ignoreSilentSwitch={"obey"}
            />
            <KeyboardAwareScrollView
                onKeyboardDidHide={() => setShowingKeyboard(false)}
                onKeyboardDidShow={() => setShowingKeyboard(true)}
                enableOnAndroid
                keyboardShouldPersistTaps="handled"
                contentContainerStyle={styles.container}>
                <View style={[styles.main, { height: showingKeyboard ? "100%" : "80%" }]}>
                    <View style={styles.left}>
                        <Text style={styles.fullName}>Елена{"\n"}Гущина</Text>
                        <Text style={styles.subHeading}>Континуум</Text>
                    </View>
                    <View style={styles.right}>
                        <View style={styles.form}>
                            {!showingKeyboard && (
                                <Text style={styles.formPrompt}>
                                    Написать пожелание можно здесь:
                                </Text>
                            )}

                            <View style={styles.inputWrapper}>
                                <LinearGradient
                                    colors={["#49CCEC", "#A422C9"]}
                                    style={[styles.gradientBorder]}
                                    useAngle>
                                    <TextInput
                                        style={[styles.input, styles.name]}
                                        placeholder="Имя"
                                        placeholderTextColor="#aaa"
                                        value={name}
                                        onChangeText={setName}
                                    />
                                </LinearGradient>
                            </View>

                            <View style={styles.inputWrapper}>
                                <LinearGradient
                                    colors={["#49CCEC", "#A422C9"]}
                                    style={styles.gradientBorder}
                                    useAngle>
                                    <TextInput
                                        style={[
                                            styles.input,
                                            styles.textArea,
                                            { height: showingKeyboard ? 200 : 280 },
                                        ]}
                                        placeholder="Текст"
                                        placeholderTextColor="#aaa"
                                        multiline
                                        value={text}
                                        onChangeText={setText}
                                    />
                                </LinearGradient>
                            </View>

                            <TouchableOpacity
                                style={[
                                    styles.button,
                                    disabledMemo ? styles.buttonDisabled : undefined,
                                ]}
                                onPress={handlePress}
                                disabled={disabledMemo}>
                                <LinearGradient
                                    colors={["#48CDEC", "#A61DC8"]}
                                    style={styles.gradient}
                                    useAngle>
                                    {sent.loading ? (
                                        <ActivityIndicator
                                            size="large"
                                            style={[styles.loading, styles.buttonText]}
                                        />
                                    ) : (
                                        <Text style={styles.buttonText}>
                                            {sent.done ? "ОТПРАВЛЕННО" : "ОТПРАВИТЬ"}
                                        </Text>
                                    )}
                                </LinearGradient>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
                {!showingKeyboard && (
                    <View style={styles.footer}>
                        <Text style={styles.leftFooter}>
                            Уважаемые друзья, мы предлагаем Вам написать тёплые пожелания для
                            именинницы, мы обязательно транслируем их на экраны и все вместе
                            поздравим Елену в этот замечательный день!
                        </Text>
                        <Text style={styles.brand}>Club 55</Text>
                    </View>
                )}
            </KeyboardAwareScrollView>
        </>
    );
};

export default SecondView;
