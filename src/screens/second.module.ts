import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
    },
    backgroundVideo: {
        ...StyleSheet.absoluteFillObject,
        zIndex: -1,
    },
    overlay: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        zIndex: 1,
    },
    absolute: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    main: {
        flexDirection: "row",
    },
    left: {
        width: "50%",
        // backgroundColor: "green",
    },
    fullName: {
        marginTop: 20,
        marginLeft: 35,
        color: "#ECBB7A",
        fontSize: 50,
        lineHeight: 60,
        fontFamily: "TT-Ricordi-Marmo-Trial-Regular", // Use your chosen font here
    },
    subHeading: {
        marginLeft: 35,
        color: "#ffffff",
        fontSize: 25,
        lineHeight: 35,
        fontFamily: "TT-Ricordi-Marmo-Trial-Regular", // Use your chosen font here
    },

    right: {
        width: "50%",
        //backgroundColor: "purple",
        marginLeft: "auto",
    },
    form: {
        alignSelf: "flex-end",
        marginTop: 20,
        marginRight: 35,
        // backgroundColor: "blue",
    },
    footer: {
        width: "100%",
        minHeight: 170,
        height: "20%",
        // backgroundColor: "red",
        display: "flex",
        flexDirection: "row",
    },
    leftFooter: {
        color: "white",
        fontSize: 21,
        textAlign: "left",
        lineHeight: 25,
        fontFamily: "Circle-Regular",
        width: 420,
        marginLeft: 35,
    },
    brand: {
        fontSize: 50,
        fontFamily: "TT-Ricordi-Marmo-Trial-Regular",
        letterSpacing: 5,
        // fontWeight: "bold",
        marginLeft: "auto",
        color: "#ECBB7A",
        marginRight: 35,
        marginTop: 65,
    },

    formPrompt: {
        color: "white",
        fontSize: 21,
        textAlign: "left",
        marginBottom: 10,
        fontFamily: "Circle-Regular",
    },
    inputWrapper: {
        width: 640,
        marginBottom: 10,
    },
    gradientBorder: {
        padding: 2,
        borderRadius: 40,
    },
    name: {
        height: 50,
    },
    input: {
        borderRadius: 40,
        paddingHorizontal: 10,
        backgroundColor: "black",
        color: "#FFFFFF",
        fontFamily: "PlayfairDisplay-Regular",
        paddingLeft: 25,
        fontSize: 17,
        lineHeight: 20,
    },
    textArea: {
        textAlignVertical: "top",
        backgroundColor: "black",
        paddingTop: 20,
    },
    button: {
        width: 639,
        borderRadius: 40,
        overflow: "hidden",
    },
    buttonDisabled: {
        opacity: 0.5,
    },
    gradient: {
        borderRadius: 40,
    },
    buttonText: {
        lineHeight: 45,
        height: 50,
        fontSize: 14,
        textAlign: "center",
        color: "white",
        fontWeight: "700",
        fontFamily: "PlayfairDisplay-Regular",
        display: "flex",
        flexDirection: "row",
    },
    loading: {
        // marginLeft: "auto",
    },
});

export default styles;
