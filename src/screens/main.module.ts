import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#323E48",
        height: "100%",
        display: "flex",
        flexDirection: "row",
    },
    leftContainer: {
        flex: 1,
        maxWidth: 740,
    },
    rightContainer: {
        flex: 1,
        backgroundColor: "#000", // Background color for the right container
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 20,
    },
    backgroundVideo: {
        ...StyleSheet.absoluteFillObject,
        zIndex: -1,
    },
    overlay: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        zIndex: 1,
    },
    leftHeading: {
        color: "#ECBB7A",
        fontSize: 50,
        lineHeight: 60,
        fontFamily: "TT-Ricordi-Marmo-Trial-Regular", // Use your chosen font here
        position: "absolute",
        top: 30,
        left: 30,
    },
    leftFooter: {
        color: "white",
        fontSize: 21,
        textAlign: "left",
        lineHeight: 25,
        marginBottom: 20,
        position: "absolute",
        bottom: 40,
        left: 30,
        fontFamily: "Circle-Regular", // Use your chosen font here
    },
    heading: {
        marginLeft: "auto",
        justifyContent: "center",
    },
    brand: {
        fontSize: 50,
        fontFamily: "TT-Ricordi-Marmo-Trial-Regular", // Use your chosen font here
        textTransform: "uppercase",
        letterSpacing: 5,
        fontWeight: "bold",
        textShadowColor: "#ECBB7A",
        textShadowRadius: 1,
        textShadowOffset: {
            width: 2,
            height: 2,
        },
        color: "black",
    },
    shadow: {
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    subheading: {
        color: "white",
        fontSize: 21,
        textAlign: "left",
        marginBottom: 10,
        fontFamily: "Circle-Regular", // Use your chosen font here
    },
    inputWrapper: {
        width: "100%",
        marginBottom: 10,
    },
    gradientBorder: {
        padding: 2,
        borderRadius: 40,
    },
    name: {
        height: 50,
    },
    input: {
        borderRadius: 40,
        paddingHorizontal: 10,
        backgroundColor: "black",
        color: "#FFFFFF",
        fontFamily: "PlayfairDisplay-Regular", // Use your chosen font here
        paddingLeft: 25,
        fontSize: 17,
        lineHeight: 20,
    },
    textArea: {
        height: 231,
        textAlignVertical: "top",
        backgroundColor: "black",
    },
    button: {
        width: "100%",
        borderRadius: 40,
        overflow: "hidden",
    },
    buttonDisabled: {
        opacity: 0.5,
    },
    gradient: {
        borderRadius: 40,
    },
    buttonText: {
        lineHeight: 45,
        height: 50,
        fontSize: 14,
        textAlign: "center",
        color: "white",
        fontWeight: "700",
        fontFamily: "PlayfairDisplay-Regular", // Use your chosen font here
    },
});

export default styles;
